# Mail Client by SNAFU

This is a simple mail client for the exercise in FTOOP in the FFHS 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You just need git to clone the repo to your local machine

```
git clone https://gitlab.com/ftoop_bachmann_schuppli/MailClient.git
```

### Installing

Important: OpenJDK users have to install javafx separately and add it to the classpath

```
apt install openjfx
```

You can use the gradle idea plugin to generate the project-file for IntelliJ IDEA

```
gradlew idea
```

Make sure to enable auto-import for the gradle project or open up the gradle window and press refresh

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [JavaFX](http://docs.oracle.com/javafx/2/overview/jfxpub-overview.htm) - Rich client applications

## Todo list

You can find a list with todos under the following location:

[Todo-list](https://gitlab.com/ftoop_bachmann_schuppli/MailClient/blob/master/todo.md)

## Authors

* **Yan Schuppli**
* **Michael Bachmann**

