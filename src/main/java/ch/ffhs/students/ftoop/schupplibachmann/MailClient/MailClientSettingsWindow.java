package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import javax.swing.*;

public class MailClientSettingsWindow
{
    public JPanel ConfigPanel;
    private JTextField emailAddressField;
    private JButton btnSaveSettings;
    private JTextField userField;
    private JTextField pop3sHostField;
    private JTextField pop3sPortField;
    private JTextField smtpHostField;
    private JTextField smtpPortField;
    private JTextField passwordField;
    private boolean exitThroughSave;

    MailClientSettingsWindow(MailClientAccountConfig mailAcc, boolean clean) {

        exitThroughSave = false;

        if (clean) {
            emailAddressField.setText("");
            userField.setText("");
            pop3sHostField.setText("");
            pop3sPortField.setText("");
            smtpHostField.setText("");
            smtpPortField.setText("");
            passwordField.setText("");
        }
        else {
            emailAddressField.setText(mailAcc.getEmailAddress());
            userField.setText(mailAcc.getUsername());
            pop3sHostField.setText(mailAcc.getReceivingHostUrl());
            pop3sPortField.setText(mailAcc.getReceivingHostPort());
            smtpHostField.setText(mailAcc.getSendingHostUrl());
            smtpPortField.setText(mailAcc.getSendingHostPort());
            passwordField.setText(mailAcc.getPassword());
        }



        // add action, so we can receiving mails
        btnSaveSettings.addActionListener(actionEvent -> {

            mailAcc.setEmailAddress(emailAddressField.getText());
            mailAcc.setUsername(userField.getText());
            mailAcc.setReceivingHostUrl(pop3sHostField.getText());
            mailAcc.setReceivingHostPort(pop3sPortField.getText());
            mailAcc.setSendingHostUrl(smtpHostField.getText());
            mailAcc.setSendingHostPort(smtpPortField.getText());
            mailAcc.setPassword(passwordField.getText());
            exitThroughSave = true;
            CloseFrame();

        });
    }

    boolean getExitThroughSave() {
        return this.exitThroughSave;
    }

    private void CloseFrame(){
        SwingUtilities.getWindowAncestor(ConfigPanel).dispose();
    }
}
