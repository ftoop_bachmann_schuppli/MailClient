package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;

public class Mail implements Serializable {

    String MailFrom;
    String MailTo;
    String MailCC;
    String MailBCC;
    String Subject;
    String HTMLContent;
    String TextContent;
    Date MailReceived;
    Date MailSent;
    boolean isNew;
    LinkedList<Attachment> Attachments;

    public Mail() {

        Attachments = new LinkedList<>();
    }

    public Mail(String MailTo) {
        this("", MailTo, "", "", "", "", "", null, null, false);
    }

    public Mail(String MailTo, String Subject) {
        this("", MailTo, "", "", Subject, "", "", null, null, false);
    }

    public Mail(
            String MailFrom,
            String MailTo,
            String MailCC,
            String MailBCC,
            String Subject,
            String HTMLContent,
            String TextContent,
            Date MailReceived,
            Date MailSent,
            boolean isNew) {

        this.MailFrom = MailFrom;
        this.MailTo = MailTo;
        this.MailCC = MailCC;
        this.MailBCC = MailBCC;
        this.Subject = Subject;
        this.HTMLContent = HTMLContent;
        this.TextContent = TextContent;
        this.MailReceived = MailReceived;
        this.MailSent = MailSent;
        this.Attachments = new LinkedList<>();
        this.isNew = isNew;
    }
}
