package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import ch.ffhs.students.ftoop.schupplibachmann.MailClient.Utils.UIHelper;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.TimerTask;

public class MailClientWindow {

    // Account config
    private static MailClientAccountConfig mailAccountConfig;

    // UI
    private static JFrame frame;
    private JPanel MainPanel;
    private JTree treeMailFolders2;
    private JList lstMails;
    private JButton btnNewMail;
    private JButton btnGetMails;
    private JLabel lblError;
    private JButton btnMailClientSettings;
    private JPanel panelCardlayout;
    private ReadMailView readMailView1;
    private JScrollPane scrollLstMails;
    private ReadMailView readMailView;

    // Popop-Menu for mail
    private JPopupMenu mailPopupMenu;
    private Mail selectedMail;

    // UI font
    private String fontFamily = "Calibri";
    private int fontStyle = Font.PLAIN;
    private int fontSize = 18;

    // Mail-stuff
    private static MailHandler mailHandler;
    private TimerTask periodicMailReceiveTask;
    private java.util.Timer periodicMailReceiveTimer;

    public static void main(String[] args) {

        frame = new JFrame("Mail Client by SNAFU - the only real one");

        // set the window icon
        try {
            frame.setIconImage(ImageIO.read(ClassLoader.getSystemResource("mailIcon.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        MailClientWindow mainWindow = new MailClientWindow();

        frame.setMinimumSize(new Dimension(1000, -1));
        frame.setContentPane(mainWindow.MainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private MailClientWindow() {

        periodicMailReceiveTask = new TimerTask() {
            @Override
            public void run() {
                mailHandler.ReceiveMails();
            }
        };

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Menu");
        JMenuItem menuItemAddAccount = new JMenuItem("Add account");
        JMenuItem menuItemExit = new JMenuItem("Exit");

        menuItemAddAccount.addActionListener(e -> addAccount());

        menuItemExit.addActionListener(e -> frame.dispose());

        menu.add(menuItemAddAccount);
        menu.add(menuItemExit);

        menuBar.add(menu);

        frame.setJMenuBar(menuBar);

        // create a blank panel for no mail selected
        JPanel panelBlank = new JPanel();
        panelBlank.setBackground(new Color(255,255,255));

        // add it to the card layout
        panelCardlayout.add(panelBlank, "blank");


        // load mail account config
        mailAccountConfig = MailClientAccountConfig.LoadConfig();
        if (mailAccountConfig != null) {
            loadAccount();
        }
        else {
            addAccount();
        }

        // and show blank
        showMailPanel(false);
        lstMails.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println(e);

                int selectedIndex = lstMails.locationToIndex(e.getPoint());
                selectedMail = (Mail) lstMails.getModel().getElementAt(selectedIndex);
                lstMails.setSelectedIndex(selectedIndex);

                // a right click
                if (SwingUtilities.isRightMouseButton(e))
                {
                    mailPopupMenu.show(e.getComponent(), e.getX(), e.getY());
                }

                // a double click
                if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2)
                {
                    JFrame showMailWindow = new JFrame("Mail lesen");
                    ReadMailView readMailView = new ReadMailView();
                    readMailView.setMail(selectedMail);
                    readMailView.calledExtern = true;
                    showMailWindow.setMinimumSize(new Dimension(500, 500));
                    showMailWindow.setPreferredSize(new Dimension(800, 600));
                    showMailWindow.setContentPane(readMailView.panel1);
                    showMailWindow.pack();
                    showMailWindow.setVisible(true);
                }
            }



        });
    }

    static MailHandler getMailHandler() {
        return mailHandler;
    }

    private void addAccount() {
        MailClientAccountConfig conf = new MailClientAccountConfig();
        MailClientSettingsWindow settingsWindow = new MailClientSettingsWindow(conf, true);

        JDialog dialog = new JDialog(frame, "Account erstellen", true);
        dialog.setContentPane(settingsWindow.ConfigPanel);
        dialog.pack();
        dialog.setVisible(true);

        // make an instance of our gmail account
        if (settingsWindow.getExitThroughSave()) {
            mailAccountConfig = conf;
            MailClientAccountConfig.SaveConfig(mailAccountConfig);
            loadAccount();
        }
        else {
            setMailClientPanelEnabled(MainPanel, false);
        }
    }

    private void loadAccount() {
        mailHandler = new MailHandler(mailAccountConfig);

        // set Inbox as actual selection
        lstMails.setModel(mailAccountConfig.folderManager.getInbox());

        // the tree for the folders
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("ROOT");

        mailPopupMenu = new JPopupMenu();
        JMenuItem menuitem = new JMenuItem("Löschen");
        menuitem.addActionListener(e -> {
            ((MailFolder)lstMails.getModel()).removeMail(selectedMail);
            MailClientAccountConfig.SaveConfig(mailAccountConfig);
        });
        mailPopupMenu.add(menuitem);

        JMenu moveToMenuItem = new JMenu("Verschieben nach");

        // add all folder from foldermanager
        Iterator it = mailAccountConfig.folderManager.mailFolders.keySet().iterator();
        int i = 0;
        while(it.hasNext()) {
            MailFolder folder = mailAccountConfig.folderManager.mailFolders.get(it.next());
            root.insert(new DefaultMutableTreeNode(folder.Name), i);
            i++;

            // add folders to moveto popop menu
            menuitem = new JMenuItem(folder.Name);
            menuitem.addActionListener(e -> {
                mailAccountConfig.folderManager.mailFolders.get(folder.Name).addMail(selectedMail);
                ((MailFolder)lstMails.getModel()).removeMail(selectedMail);
                MailClientAccountConfig.SaveConfig(mailAccountConfig);
            });
            moveToMenuItem.add(menuitem);
        }
        mailPopupMenu.add(moveToMenuItem);

        // Create the tree model and add the root node to it
        DefaultTreeModel model = new DefaultTreeModel(root);

        // get renderer of tree
        DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) treeMailFolders2.getCellRenderer();

        // load the folder icons for the tree
        Icon iconFolderOpen = new ImageIcon(ClassLoader.getSystemResource("folderOpen32x32.png"));
        Icon iconFolderClosed = new ImageIcon(ClassLoader.getSystemResource("folderClosed32x32.png"));

        // and set them
        renderer.setClosedIcon(iconFolderClosed);
        renderer.setOpenIcon(iconFolderOpen);
        renderer.setLeafIcon(iconFolderClosed);

        // Create the tree with the new model
        treeMailFolders2.setModel(model);
        treeMailFolders2.setMinimumSize(new Dimension(150, 0));
        // listener for catching click events to change actual mail folder diplayed
        treeMailFolders2.addTreeSelectionListener(treeSelectionEvent -> {

            //Returns the last path element of the selection.
            //This method is useful only when the selection model allows a single selection.
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeMailFolders2.getLastSelectedPathComponent();

            if (node == null)
                //Nothing is selected.
                return;

            String selectedFolder = (String)node.getUserObject();

            lstMails.setModel(mailAccountConfig.folderManager.mailFolders.get(selectedFolder));
        });

        treeMailFolders2.setSelectionPath(find((DefaultMutableTreeNode) treeMailFolders2.getModel().getRoot(), mailAccountConfig.folderManager.getInbox().Name));

        // register on the mailsReceived event on our mail account, so we can update our inbox
        mailHandler.addNewMailsReceivedListener(
                e -> {
                    for(Mail mail: e.Messages()) {
                        mailAccountConfig.folderManager.getInbox().addMail(mail);
                    }
                    MailClientAccountConfig.SaveConfig(mailAccountConfig);
                }
        );

        // register on the errorHappend event on our mail account, so we can show some errors
        mailHandler.addErrorHappenedListener(
                e -> JOptionPane.showMessageDialog(frame,
                        e.getError(),
                        "Error",
                        JOptionPane.ERROR_MESSAGE)
        );

        periodicMailReceiveTimer = new java.util.Timer();
        periodicMailReceiveTimer.scheduleAtFixedRate(periodicMailReceiveTask, 0, 60*1000);

        // add action, so we can receiving mails
        btnGetMails.addActionListener(actionEvent -> mailHandler.ReceiveMails());

        // add action, so we can send mails
        btnNewMail.addActionListener(actionEvent -> {
            JFrame sendFrame = new JFrame("Neue E-Mail senden");
            sendFrame.setContentPane(new NewMailWindow().SendPanel);
            sendFrame.pack();
            sendFrame.setVisible(true);
        });


        btnMailClientSettings.addActionListener(actionEvent -> {
            JFrame configFrame = new JFrame("Mail Client Konfigurieren");
            configFrame.setContentPane(new MailClientSettingsWindow(mailAccountConfig, false).ConfigPanel);
            configFrame.pack();
            configFrame.setVisible(true);
        });

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                MailClientAccountConfig.SaveConfig(mailAccountConfig);
            }
        });

        setMailClientPanelEnabled(MainPanel, true);
    }

    // Here goes every GUI-element which has "Custom Create" set to TRUE in the GUI editor
    private void createUIComponents() {

        // set the default font
        UIHelper.setUIFont(new javax.swing.plaf.FontUIResource(fontFamily,fontStyle,fontSize));
        treeMailFolders2 = new JTree();
        // the list for the mails
        lstMails = new JList();
        lstMails.setCellRenderer(new MailCellRenderer());
        lstMails.addListSelectionListener(listSelectionEvent -> {

            // check if anything is selected
            if (lstMails.getSelectedIndex() >= 0) {

                // get the selected mail in the list view
                Mail mail = (Mail) lstMails.getModel().getElementAt(lstMails.getSelectedIndex());
                ((Mail) lstMails.getModel().getElementAt(lstMails.getSelectedIndex())).isNew = false;

                // activate the mail view
                showMailPanel(true);

                // set content on the view
                readMailView.setMail(mail);
            }
            else {
                // deactivate the mail view (blank)
                showMailPanel(false);
            }
        });
    }

    // switches between blank and mail view on main frame
    private void showMailPanel(boolean show) {
        if (show) {
            ((CardLayout)panelCardlayout.getLayout()).show(panelCardlayout, "mail");
        }
        else {
            ((CardLayout) panelCardlayout.getLayout()).show(panelCardlayout, "blank");
        }
    }


    // find a node in a tree recursive and return its path
    private TreePath find(DefaultMutableTreeNode root, String s) {

        Enumeration e = root.depthFirstEnumeration();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
            if (node.toString().equalsIgnoreCase(s)) {
                return new TreePath(node.getPath());
            }
        }
        return null;
    }

    private void setMailClientPanelEnabled(Container parent, boolean enabled)
    {
        for(Component c : parent.getComponents())
        {
            if(c instanceof Container)
            {
                c.setEnabled(enabled);

                setMailClientPanelEnabled((Container)c, enabled);
            }
        }
    }
}