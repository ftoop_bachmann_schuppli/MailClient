package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import java.io.Serializable;
import java.util.HashMap;

class MailFolderManager implements Serializable {

    // Constants for default folders
    private static final String INBOX_NAME = "Inbox";
    private static final String DRAFTS_NAME = "Drafts";
    private static final String OUTBOX_NAME = "Outbox";
    private static final String SENT_NAME = "Sent";

    HashMap<String, MailFolder> mailFolders;

    MailFolderManager() {
        mailFolders = new HashMap<>();
        mailFolders.put(INBOX_NAME, new MailFolder(INBOX_NAME, this));
        mailFolders.put(DRAFTS_NAME, new MailFolder(DRAFTS_NAME, this));
        mailFolders.put(OUTBOX_NAME, new MailFolder(OUTBOX_NAME, this));
        mailFolders.put(SENT_NAME, new MailFolder(SENT_NAME, this));
    }

    MailFolder getInbox() {
        return mailFolders.get(INBOX_NAME);
    }
}
