package ch.ffhs.students.ftoop.schupplibachmann.MailClient.Utils;

import java.awt.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class DesktopUtils {

    // Desktop
    private static Desktop desktop;
    private static DesktopUtils instance;

    private DesktopUtils() {
        // Desktop for open links in browser
        desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
    }

    public static synchronized DesktopUtils getInstance() {

        if (instance == null)
            instance = new DesktopUtils();
        return instance;
    }

    private void openWebpage(URI uri) {

        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void openWebpage(URL url) {

        try {
            openWebpage(url.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void openWebpage(String url) {

        try {
            openWebpage(new URL(url));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
