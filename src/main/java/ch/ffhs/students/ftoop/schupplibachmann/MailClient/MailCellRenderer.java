package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import ch.ffhs.students.ftoop.schupplibachmann.MailClient.Utils.DateFormatter;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

class MailCellRenderer extends JPanel implements ListCellRenderer<Mail> {


    private Border newMailBorder;
    private Border emptyBorder;
    private JLabel mailSubject;
    private JLabel mailDate;
    private JLabel mailFrom;
    private JLabel mailShortText;
    private JPanel newIndicatorPanel;

    MailCellRenderer() {

        setLayout(new BorderLayout());

        mailDate = new JLabel();
        mailFrom = new JLabel();
        mailSubject = new JLabel();
        mailShortText = new JLabel();

        JPanel infoPanel = new JPanel(new GridLayout(0,1));
        infoPanel.add(mailFrom);
        infoPanel.add(mailSubject);
        infoPanel.add(mailShortText);


        JPanel spacerPanel = new JPanel();
        spacerPanel.setMinimumSize(new Dimension(20,50));
        spacerPanel.setMaximumSize(new Dimension(20,50));

        JPanel innerPanel = new JPanel(new BorderLayout());
        innerPanel.add(infoPanel, BorderLayout.CENTER);
        innerPanel.add(mailDate, BorderLayout.EAST);
        add(innerPanel, BorderLayout.CENTER);

        newIndicatorPanel = new JPanel();
        newIndicatorPanel.setMinimumSize(new Dimension(20,50));
        newIndicatorPanel.setMaximumSize(new Dimension(20,50));


        // Swing labels default to being transparent; the container's color
        // shows through. To change a Swing label's background color, you must
        // first make the label opaque (by passing true to setOpaque()). Later,
        // you invoke setBackground(), passing the new color as the argument.
        setOpaque(true);

        // This newMailBorder is placed around a cell that is selected and has focus.
        newMailBorder = BorderFactory.createMatteBorder(0, 3, 0, 0, Color.BLUE);
        Border dividerBoder = BorderFactory.createMatteBorder(0, 0, 1, 0, Color.lightGray);
        emptyBorder = BorderFactory.createEmptyBorder();
        this.setBorder(dividerBoder);

        add(spacerPanel, BorderLayout.EAST);
        add(newIndicatorPanel, BorderLayout.WEST);
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Mail> jList, Mail value, int index, boolean isSelected, boolean cellHasFocus) {

        mailFrom.setText(value.MailFrom);
        mailSubject.setText(value.Subject);
        mailDate.setText(DateFormatter.getNiceDateString(value.MailSent));

        // get first 100 chars of mail content
        int endIndex = 100;
        if (value.TextContent.length() < 100) {
            endIndex = value.TextContent.length();
            mailShortText.setText(value.TextContent.substring(0, endIndex));
        }
        else {
            mailShortText.setText(value.TextContent.substring(0, endIndex)+"...");
        }


        if(value.isNew) {
            newIndicatorPanel.setBorder(newMailBorder);
        }
        else {
            newIndicatorPanel.setBorder(emptyBorder);
        }

        if (isSelected) {
            setPanelColor(this, jList.getSelectionBackground(), jList.getSelectionForeground());
        }
        else {
            setPanelColor(this, jList.getBackground(), jList.getForeground());
        }

        setFont(jList.getFont());
        setEnabled(jList.isEnabled());

        return this;
    }

    private void setPanelColor(Container parent, Color bgColor, Color fgColor)
    {
        for(Component c : parent.getComponents())
        {
            if(c instanceof Container)
            {
                if(c instanceof JPanel)
                {
                    c.setBackground(bgColor);
                    c.setForeground(fgColor);
                }

                setPanelColor((Container)c, bgColor, fgColor);
            }
        }
    }
}
