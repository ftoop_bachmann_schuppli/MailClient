package ch.ffhs.students.ftoop.schupplibachmann.MailClient.Utils;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {

    public static String getNiceDateTimeString(Date date) {
        if (date != null)
            return DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM, Locale.getDefault()).format(date);
        return "";
    }

    public static String getNiceDateString(Date date) {
        if (date != null)
            return DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault()).format(date);
        return "";
    }
}
