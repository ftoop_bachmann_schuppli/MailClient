package ch.ffhs.students.ftoop.schupplibachmann.MailClient.Utils;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.UrlValidator;

public class Validator {

    public static boolean isValidMail(String email) {
        if (email == null || "".equals(email))
            return false;

        email = email.trim();
        if (email.indexOf("mailto:") == 0)
            email = email.substring(7,email.length());
        email = email.trim();

        EmailValidator ev = EmailValidator.getInstance();
        return ev.isValid(email);

    }

    public static boolean isValidURL(String url) {
        if (url == null || "".equals(url))
            return false;

        url = url.trim();

        UrlValidator uv = UrlValidator.getInstance();
        return uv.isValid(url);
    }
}
