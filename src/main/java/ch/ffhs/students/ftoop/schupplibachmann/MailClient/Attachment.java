package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Paths;

class Attachment implements Serializable {

    private String Filename;
    private String MimeType;
    private byte[] AttachementBytes;

    String getMimeType() {
        return MimeType;
    }

    byte[] getAttachementBytes() {
        return AttachementBytes;
    }

    String getFilename() {
        return Filename;
    }

    Attachment(String Filename, String MimeType, byte[] AttachementBytes) {

        this.Filename = Filename;
        this.MimeType = MimeType;
        this.AttachementBytes = AttachementBytes;
    }

    void SaveAttachement(String dirPath) throws IOException {

        String filePath = Paths.get(dirPath, this.Filename).toString();
        FileOutputStream fos = new FileOutputStream(filePath);
        fos.write(this.AttachementBytes);
        fos.close();
    }
}
