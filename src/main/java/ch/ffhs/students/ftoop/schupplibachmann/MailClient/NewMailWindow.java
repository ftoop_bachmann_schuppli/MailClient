package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedList;

public class NewMailWindow
{
    public JPanel SendPanel;
    private JTextField toEmailField;
    private JTextField ccEmailField;
    private JTextField subjectEmailField;
    private JButton sendButton;
    private JTextArea tareaSendText;
    private JTextField bccEmailField;
    private JTabbedPane tabbedPane1;
    private JTextArea tareaHtml;
    private JButton addAttachmentButton;

    private LinkedList<Attachment> attachments;

    NewMailWindow() {
        this(null);
    }

    NewMailWindow(Mail mail) {
        attachments = new LinkedList<>();

        addAttachmentButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Load attachment");
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setAcceptAllFileFilterUsed(true);
            if (fileChooser.showOpenDialog(SendPanel) == JFileChooser.APPROVE_OPTION) {
                File attachmentFile = fileChooser.getSelectedFile();
                try {
                    byte[] fileContent = org.apache.commons.io.IOUtils.toByteArray(new FileInputStream(attachmentFile));
                    Attachment attachment = new Attachment(attachmentFile.getName(), Files.probeContentType(attachmentFile.toPath()), fileContent);
                    attachments.add(attachment);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        if (mail != null) {
            if (mail.Subject != null && !mail.Subject.equals(""))
                subjectEmailField.setText(mail.Subject);
            if (mail.MailTo != null && !mail.MailTo.equals(""))
                toEmailField.setText(mail.MailTo);
            if (mail.MailCC != null && !mail.MailCC.equals(""))
                ccEmailField.setText(mail.MailCC);
            if (mail.MailBCC != null && !mail.MailBCC.equals(""))
                bccEmailField.setText(mail.MailBCC);
            if (mail.TextContent != null && !mail.TextContent.equals(""))
                tareaSendText.setText(mail.TextContent);
            if (mail.HTMLContent != null && !mail.HTMLContent.equals(""))
                tareaHtml.setText(mail.HTMLContent);
            if (mail.Attachments != null && mail.Attachments.size() > 0)
                attachments.addAll(mail.Attachments);

            tareaSendText.setCaretPosition(0);
            tareaHtml.setCaretPosition(0);
        }

        // add action, so we can receiving mails
        sendButton.addActionListener(actionEvent -> {

            Mail newMail = new Mail();

            if (toEmailField.getText().equals(""))  {
                JOptionPane.showMessageDialog(null, "An Feld ist leer!", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }

            newMail.MailTo = toEmailField.getText();
            newMail.MailCC = ccEmailField.getText();
            newMail.MailBCC = bccEmailField.getText();
            newMail.Subject = subjectEmailField.getText();
            newMail.TextContent = tareaSendText.getText();
            newMail.HTMLContent = tareaHtml.getText();
            newMail.Attachments.addAll(attachments);


            MailClientWindow.getMailHandler().SendMail(newMail);
            Window parent = SwingUtilities.getWindowAncestor(SendPanel);
            parent.dispose();
        });
    }

    private void createUIComponents() {
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        CompoundBorder compBorder = BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(5, 5, 5, 5));
        tareaSendText = new JTextArea();
        tareaSendText.setBorder(compBorder);

        toEmailField = new JTextField();
        toEmailField.setBorder(compBorder);
        ccEmailField = new JTextField();
        ccEmailField.setBorder(compBorder);
        bccEmailField = new JTextField();
        bccEmailField.setBorder(compBorder);
        subjectEmailField = new JTextField();
        subjectEmailField.setBorder(compBorder);
    }
}
