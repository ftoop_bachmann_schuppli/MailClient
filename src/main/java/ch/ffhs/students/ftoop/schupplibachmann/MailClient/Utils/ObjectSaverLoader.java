package ch.ffhs.students.ftoop.schupplibachmann.MailClient.Utils;

import java.io.*;

public class ObjectSaverLoader {

    public static void serialize(Object object, String savePath) {

        File dir = new File(savePath).getParentFile();

        if (!dir.exists())
            dir.mkdirs();

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(savePath))) {

            oos.writeObject(object);
            System.out.println("Object serialized to: " + savePath);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static <T> T deserialize(Class<T> classType, String savePath) {

        File file = new File(savePath);
        if (file.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(savePath))) {

                Object obj = ois.readObject();
                if (classType.isInstance(obj)) {
                    return classType.cast(obj);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
