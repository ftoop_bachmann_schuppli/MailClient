package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.util.LinkedList;

class MailContentBuilder {

    Multipart build(String messageText, String messageHtml, LinkedList<Attachment> attachments) throws MessagingException {

        final Multipart mpMixed = new MimeMultipart("mixed");

        // alternative
        final Multipart mpMixedAlternative = newChild(mpMixed, "alternative");

        // Note: MUST RENDER HTML LAST otherwise iPad mail client only renders the last image and no email
        addTextVersion(mpMixedAlternative,messageText);
        addHtmlVersion(mpMixedAlternative,messageHtml);

        // Attachments
        addAttachments(mpMixed,attachments);

        return mpMixed;
    }

    private Multipart newChild(Multipart parent, String alternative) throws MessagingException {

        MimeMultipart child =  new MimeMultipart(alternative);
        final MimeBodyPart mbp = new MimeBodyPart();
        parent.addBodyPart(mbp);
        mbp.setContent(child);
        return child;
    }

    private void addTextVersion(Multipart mpRelatedAlternative, String messageText) throws MessagingException {

        final MimeBodyPart textPart = new MimeBodyPart();
        textPart.setContent(messageText, "text/plain");
        mpRelatedAlternative.addBodyPart(textPart);
    }

    private void addHtmlVersion(Multipart parent, String messageHtml) throws MessagingException {

        // HTML version
        final Multipart mpRelated = newChild(parent,"related");

        // Html
        final MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(messageHtml, "text/html");
        mpRelated.addBodyPart(htmlPart);
    }

    private void addAttachments(Multipart parent, LinkedList<Attachment> attachments) throws MessagingException {

        if (attachments != null)
        {
            for (Attachment attachment : attachments)
            {
                final MimeBodyPart mbpAttachment = new MimeBodyPart();
                DataSource attachmentSource = new ByteArrayDataSource(attachment.getAttachementBytes(), attachment.getMimeType());
                mbpAttachment.setDataHandler(new DataHandler(attachmentSource));
                String fileName = getFileName(attachment.getFilename());
                mbpAttachment.setDisposition(BodyPart.ATTACHMENT);
                mbpAttachment.setFileName(fileName);
                parent.addBodyPart(mbpAttachment);
            }
        }
    }

    private String getFileName(String fileName) {

        if (fileName.contains("/"))
            fileName = fileName.substring(fileName.lastIndexOf("/")+1);
        return fileName;
    }
}
