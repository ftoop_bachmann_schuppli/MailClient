package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import org.apache.commons.io.IOUtils;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.*;
import javax.swing.event.EventListenerList;
import java.io.InputStream;
import java.util.*;

class MailHandler {

    enum SendProtocol {SMTP, SMTPS}
    enum ReceiveProtocol {POP3, POP3S, IMAP, IMAPS}

    private MailClientAccountConfig config;
    private String sendProtocolToUse;
    // properties
    private Properties props;

    MailHandler(MailClientAccountConfig config)
    {
        this.config = config;
        SetupSending();
        SetupReceiving();
    }

    private void SetupReceiving() {

        // Get system properties
        if (props == null)
            props = System.getProperties();

        props.setProperty("mail.host", this.config.receivingHostUrl);

        switch (this.config.receivingHostProtocol) {

            case POP3:
                props.setProperty("mail.pop3.auth", "true");
                props.setProperty("mail.pop3.port", this.config.receivingHostPort);
                props.setProperty("mail.store.protocol", "pop3");


            case POP3S:
                props.setProperty("mail.pop3s.auth", "true");
                props.setProperty("mail.pop3s.port", this.config.receivingHostPort);
                props.setProperty("mail.store.protocol", "pop3s");
        }
    }

    private void SetupSending() {

        // Get system properties
        if (props == null)
            props = System.getProperties();

        // set generic settings
        props.setProperty("mail.smtp.host", this.config.sendingHostUrl);
        props.setProperty("mail.smtp.port", this.config.sendingHostPort);
        props.setProperty("mail.smtp.auth", "true");
        sendProtocolToUse = "smtp";

        // Setup settings specific to protocol
        switch (this.config.sendHostProtocol) {

            case SMTP:
                // disable tls
                props.setProperty("mail.smtp.starttls.enable", "false");
                break;

            case SMTPS:
                // enable tls
                props.setProperty("mail.smtp.starttls.enable", "true");
                break;
        }
    }

    void SendMail(Mail mail) {

        new SwingWorker() {

            @Override
            protected Integer doInBackground() {
                try {
                    // Get the default Session object
                    Session session = Session.getDefaultInstance(props);

                    // Create a default MimeMessage object
                    MimeMessage message = new MimeMessage(session);

                    // Set From: header field of the header
                    message.setFrom(new InternetAddress(config.emailAddress));

                    // Set To: header field of the header
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(mail.MailTo));

                    // Set Subject: header field
                    message.setSubject(mail.Subject);

                    MailContentBuilder mailContentBuilder = new MailContentBuilder();
                    Multipart mpMixed = mailContentBuilder.build(
                            mail.TextContent,
                            mail.HTMLContent,
                            mail.Attachments);

                    // Set the Multipart's to be the email's content
                    message.setContent(mpMixed);
                    message.setSentDate(new Date());

                    Transport transport = session.getTransport(sendProtocolToUse);

                    transport.connect(config.username, config.password);
                    transport.sendMessage(message, message.getAllRecipients());
                    transport.close();

                    System.out.println("Message sent");

                    return 1;
                }
                catch (Exception e) {
                    e.printStackTrace();
                    fireErrorHappend(new ErrorHappendEvent(this, e.getMessage()));
                    return 0;
                }
            }

            @Override
            public void done()
            {
                fireMailSent(new MailSentEvent(this, mail));
            }
        }.execute();
    }

    void ReceiveMails() {

        new SwingWorker() {

            LinkedList<Mail> newMailsList = new LinkedList<>();

            @Override
            protected Integer doInBackground() {
                try {
                    // Get the default Session object
                    Session session = Session.getDefaultInstance(props);
                    // Get a store for the POP3S protocol
                    Store store = session.getStore();

                    // Connect to the current receivingHostUrl using the specified username and password
                    store.connect(config.username, config.password);

                    // Create a Folder object corresponding to the given name
                    Folder folder = store.getFolder("inbox");

                    // Open the Folder
                    folder.open(Folder.READ_WRITE);

                    // Get the messages from the server
                    Message[] newMails = folder.getMessages();

                    for (Message msg : newMails) {
                        // create mail object without the content for further processing
                        Mail mail = new Mail(
                                InternetAddress.toString(msg.getFrom()),
                                InternetAddress.toString(msg.getRecipients(Message.RecipientType.TO)),
                                InternetAddress.toString(msg.getRecipients(Message.RecipientType.CC)),
                                InternetAddress.toString(msg.getRecipients(Message.RecipientType.BCC)),
                                msg.getSubject(),
                                "",
                                "",
                                msg.getReceivedDate(),
                                msg.getSentDate(),
                                true
                        );

                        // handle the content of the mail
                        mail = handleContentType(msg, mail);

                        // add the resulting mail to the received list
                        newMailsList.add(mail);

                        //DEBUG output
                        System.out.println("From: " + mail.MailFrom);
                        System.out.println("To: " + mail.MailTo);
                        System.out.println("CC: " + mail.MailCC);
                        System.out.println("BCC: " + mail.MailBCC);
                        System.out.println("Subject: " + mail.Subject);
                        System.out.println("Received: " + mail.MailReceived);
                        System.out.println("Sent: " + mail.MailSent);
                        System.out.println();
                        System.out.println("Text-Content:");
                        System.out.println(mail.TextContent);
                        System.out.println();
                        System.out.println("HTML-Content:");
                        System.out.println(mail.HTMLContent);

                        // TODO: proper handling of attachement saving

                        /* In der endgueltigen Version sollen die Mails geloest werden */

                        // Mark this message for deletion when the session is closed
                        // msg.setFlag( Flags.Flag.DELETED, true ) ;
                    }

                    folder.close(true);
                    store.close();

                    return newMailsList.size();
                }
                catch (Exception e) {
                    e.printStackTrace();
                    fireErrorHappend(new ErrorHappendEvent(this, e.getMessage()));
                    return 0;
                }
            }

            @Override
            public void done()
            {
                fireReceivedNewMails(new NewMailsReceivedEvent(this, newMailsList));
            }
        }.execute();
    }

    private Mail handleContentType(Part msg, Mail mail) throws Exception {

        // get the content type of the part
        String contentType = msg.getContentType();

        // handle the part coresponding to its type
        if (contentType.contains("multipart")) {

            Multipart multipart = (Multipart) msg.getContent();

            for (int j = 0; j < multipart.getCount(); j++) {

                BodyPart bodyPart = multipart.getBodyPart(j);
                String disposition = bodyPart.getDisposition();

                if (disposition != null && disposition.equalsIgnoreCase(Part.ATTACHMENT)) {
                    System.out.println("Mail have some attachment");

                    // name of attachement
                    DataHandler handler = bodyPart.getDataHandler();
                    System.out.println("file name : " + handler.getName());

                    // save the attachement to a byte-array
                    InputStream is = bodyPart.getInputStream();
                    byte[] fileContent = IOUtils.toByteArray(is);

                    // and add it to the mail
                    mail.Attachments.add(new Attachment(handler.getName(), handler.getContentType(), fileContent));
                }
                else {
                    // process the part
                    mail = handleContentType(bodyPart, mail);
                }
            }
        }
        else if (contentType.contains("text/plain")) {

            if (contentType.contains("UTF-8")) {

                // the content in the mail is UTF-8
                // TODO: Is handling of UTF-8 done correctly?
            }
            // otherwise, we assume that it's US-ASCII
            mail.TextContent = (String)msg.getContent();
        }
        else if (contentType.contains("text/html")) {

            if (contentType.contains("UTF-8")) {

                // the content in the mail is UTF-8
                // TODO: Is handling of UTF-8 done correctly?
            }
            // otherwise, we assume that it's US-ASCII
            mail.HTMLContent = (String)msg.getContent();
        }
        // is it a multipart mail?
        else {
            // looks like a attached picture for the email content
            // TODO: implementation of images for the mail content
        }
        return mail;
    }

    private EventListenerList newMailsReceivedListenerList = new EventListenerList();
    private EventListenerList MailSentListenerList = new EventListenerList();
    private EventListenerList ErrorHappendListenerList = new EventListenerList();

    void addNewMailsReceivedListener(NewMailsReceivedListener l) {
        newMailsReceivedListenerList.add(NewMailsReceivedListener.class, l);
    }

    void removeNewMailsReceivedListener(NewMailsReceivedListener l) {
        newMailsReceivedListenerList.remove(NewMailsReceivedListener.class, l);
    }

    void addMailSentListener(NewMailsReceivedListener l) {
        MailSentListenerList.add(NewMailsReceivedListener.class, l);
    }

    void removeMailListListener(NewMailsReceivedListener l) {
        MailSentListenerList.remove(NewMailsReceivedListener.class, l);
    }

    void addErrorHappenedListener(ErrorHappendListener l) {
        ErrorHappendListenerList.add(ErrorHappendListener.class, l);
    }

    void removeErrorHappenedListener(ErrorHappendListener l) {
        ErrorHappendListenerList.remove(ErrorHappendListener.class, l);
    }

    // Roughly analogous to .net OnEvent protected virtual method pattern -
    // call this method to raise the event
    private void fireReceivedNewMails(NewMailsReceivedEvent e) {

        NewMailsReceivedListener[] ls = newMailsReceivedListenerList.getListeners(NewMailsReceivedListener.class);
        for (NewMailsReceivedListener l : ls) {
            l.NewMailsReceived(e);
        }
    }

    private void fireMailSent(MailSentEvent e) {

        MailSentListener[] ls = MailSentListenerList.getListeners(MailSentListener.class);
        for (MailSentListener l : ls) {
            l.MailSent(e);
        }
    }

    private void fireErrorHappend(ErrorHappendEvent e) {

        ErrorHappendListener[] ls = ErrorHappendListenerList.getListeners(ErrorHappendListener.class);
        for (ErrorHappendListener l : ls) {
            l.ErrorHappend(e);
        }
    }

    class NewMailsReceivedEvent extends EventObject {

        private LinkedList<Mail> _messages;

        NewMailsReceivedEvent(Object source, LinkedList<Mail> messages) {

            super( source );
            _messages = messages;
        }
        LinkedList<Mail> Messages() {
            return _messages;
        }
    }

    interface NewMailsReceivedListener extends EventListener {
        void NewMailsReceived(NewMailsReceivedEvent e);
    }

    private class MailSentEvent extends EventObject {

        private Mail mail;

        MailSentEvent(Object source, Mail mail) {

            super( source );
            this.mail = mail;
        }
        public Mail getMail() {
            return this.mail;
        }
    }

    interface MailSentListener extends EventListener {
        void MailSent(MailSentEvent e);
    }

    class ErrorHappendEvent extends EventObject {

        private String error;

        ErrorHappendEvent(Object source, String error) {

            super( source );
            this.error = error;
        }
        String getError() {
            return this.error;
        }
    }

    interface ErrorHappendListener extends EventListener {
        void ErrorHappend(ErrorHappendEvent e);
    }
}