package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import ch.ffhs.students.ftoop.schupplibachmann.MailClient.Utils.ObjectSaverLoader;
import java.io.Serializable;

class MailClientAccountConfig implements Serializable{

    private final static String SAVEPATH = "config/account.ser";

    // The url + port of the receiving server
    MailHandler.ReceiveProtocol receivingHostProtocol;
    String receivingHostUrl;
    String receivingHostPort;
    // The url + port of the sending server
    MailHandler.SendProtocol sendHostProtocol;
    String sendingHostUrl;
    String sendingHostPort;
    // Username and password
    String username;
    String password;
    // email address of the account
    String emailAddress;
    MailFolderManager folderManager;

    private MailClientAccountConfig(String emailAddress,
                                    String username,
                                    String password,
                                    MailHandler.ReceiveProtocol receivingHostProtocol,
                                    String receivingHostUrl,
                                    int receivingHostPort,
                                    MailHandler.SendProtocol sendHostProtocol,
                                    String sendingHostUrl,
                                    int sendingHostPort,
                                    MailFolderManager folderManager)
    {
        this.emailAddress = emailAddress;
        this.username = username;
        this.password = password;

        this.receivingHostProtocol = receivingHostProtocol;
        this.receivingHostUrl = receivingHostUrl ;
        this.receivingHostPort = Integer.toString(receivingHostPort);

        this.sendHostProtocol = sendHostProtocol;
        this.sendingHostUrl = sendingHostUrl;
        this.sendingHostPort = Integer.toString(sendingHostPort);

        this.folderManager = folderManager;
    }

    MailClientAccountConfig() {
        this("","","", MailHandler.ReceiveProtocol.POP3S, "", 1, MailHandler.SendProtocol.SMTPS, "", 1, new MailFolderManager());
    }

    static MailClientAccountConfig LoadConfig() {
        return ObjectSaverLoader.deserialize(MailClientAccountConfig.class, SAVEPATH);
    }

    static void SaveConfig(MailClientAccountConfig conf) {
        ObjectSaverLoader.serialize(conf, SAVEPATH);
    }


    // Getter und Setter Methods for Settings
    String getPassword() {
        return this.password;
    }
    void setPassword(String password) {
        this.password = password;
    }

    String getUsername() {
        return this.username;
    }
    void setUsername(String user) {
        this.username = user;
    }

    String getEmailAddress() {
        return this.emailAddress;
    }
    void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    String getReceivingHostUrl() {
        return this.receivingHostUrl;
    }
    void setReceivingHostUrl(String receivingHostUrl) {
        this.receivingHostUrl = receivingHostUrl;
    }

    String getReceivingHostPort() {
        return this.receivingHostPort;
    }
    void setReceivingHostPort(String receivingHostPort) {
        this.receivingHostPort = receivingHostPort;
    }

    String getSendingHostUrl() {
        return this.sendingHostUrl;
    }
    void setSendingHostUrl(String sendingHostUrl) {
        this.sendingHostUrl = sendingHostUrl;
    }

    String getSendingHostPort() {
        return this.sendingHostPort;
    }
    void setSendingHostPort(String sendingHostPort) {
        this.sendingHostPort = sendingHostPort;
    }
}
