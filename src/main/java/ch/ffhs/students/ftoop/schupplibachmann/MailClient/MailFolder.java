package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

class MailFolder implements Serializable, ListModel<Mail> {

    String Name;
    private ArrayList MailsList;
    private transient ArrayList<ListDataListener> listeners;
    private Object source;

    MailFolder(String Name, Object source) {

        this.Name = Name;
        this.source = source;
        listeners = new ArrayList<>();
        MailsList = new ArrayList<>();
    }

    void addMail(Mail mail) {

        // TODO: add mail at correct position (date received)
        MailsList.add(0, mail);
        notifyListeners();
    }

    void removeMail(Mail mail) {

        if (MailsList.remove(mail)) {
            notifyListeners();
        }
    }

    private void notifyListeners() {

        // TODO: optimization, so that only the changed element(s) will be updated
        ListDataEvent le = new ListDataEvent(this.source, ListDataEvent.CONTENTS_CHANGED, 0, getSize());
        for (ListDataListener listener : listeners) {
            listener.contentsChanged(le);
        }
    }

    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        listeners = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Mail folder '" + this.Name + "' " +
                "with " + this.MailsList.size() + " mails";
    }

    @Override
    public int getSize() {
        return MailsList.size();
    }

    @Override
    public Mail getElementAt(int i) {
        return (Mail)MailsList.get(i);
    }

    @Override
    public void addListDataListener(ListDataListener listDataListener) {

        if (listeners == null)
            listeners = new ArrayList<>();

        listeners.add(listDataListener);
    }

    @Override
    public void removeListDataListener(ListDataListener listDataListener) {
        listeners.remove(listDataListener);
    }
}
