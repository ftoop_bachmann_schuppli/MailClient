package ch.ffhs.students.ftoop.schupplibachmann.MailClient;

import ch.ffhs.students.ftoop.schupplibachmann.MailClient.Utils.DateFormatter;
import ch.ffhs.students.ftoop.schupplibachmann.MailClient.Utils.DeepCopy;
import ch.ffhs.students.ftoop.schupplibachmann.MailClient.Utils.DesktopUtils;
import ch.ffhs.students.ftoop.schupplibachmann.MailClient.Utils.Validator;
import javafx.application.Platform;
import javafx.concurrent.Worker;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.*;
import org.w3c.dom.html.HTMLAnchorElement;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class ReadMailView {
    private JPanel panelTextContent;
    private JButton answerButton;
    private JButton answerAllButton;
    private JButton forwardButton;
    private JTextArea taTextContent;
    public JPanel panel1;
    private WebView webView;
    private JButton replyButton;
    private JButton replyAllButton;
    private JFXPanel jfxPanelHtml;
    private JLabel lblFrom;
    private JLabel lblTo;
    private JLabel lblSentDate;
    private JLabel lblSubject;
    private JLabel lblCC;
    private JLabel lblCCTitle;
    private JTabbedPane tabbedPane1;
    private JPanel attachmentPanel;
    private JButton saveAttachmentsButton;
    private Mail mail;
    boolean calledExtern;


    ReadMailView() {
        calledExtern = false;

        forwardButton.addActionListener(e -> {
            JFrame sendFrame = new JFrame("E-Mail weiterleiten");
            Mail forwardMail = (Mail)DeepCopy.copy(mail);
            forwardMail.Subject = "Fwd: " + forwardMail.Subject;
            forwardMail.MailTo = "";
            forwardMail.MailCC = "";
            forwardMail.MailBCC = "";

            String replyText = "\r\n\r\n";
            replyText += "Begin of forwarded message:\r\n\r\n";
            replyText += "> From: " + mail.MailFrom + "\r\n";
            replyText += "> Date: " + mail.MailSent + "\r\n";
            replyText += "> To: " + mail.MailTo + "\r\n";
            replyText += "> Subject: " + mail.Subject + "\r\n";
            replyText += ">\r\n";
            for(String line: mail.TextContent.split(System.lineSeparator()))
            {
                replyText += "> " + line + "\r\n";
            }
            forwardMail.TextContent = replyText;

            // TODO: Add reply header to html

            sendFrame.setContentPane(new NewMailWindow(forwardMail).SendPanel);
            sendFrame.pack();
            sendFrame.setVisible(true);
            if (calledExtern) {
                SwingUtilities.getWindowAncestor(panel1).dispose();
            }
        });

        replyButton.addActionListener(e -> {
            JFrame sendFrame = new JFrame("Antworten auf E-Mail");
            Mail replyMail = (Mail)DeepCopy.copy(mail);
            replyMail.Subject = "Re: " + replyMail.Subject;
            replyMail.MailTo = replyMail.MailFrom;
            replyMail.MailCC = "";
            replyMail.MailBCC = "";

            String replyText = "\r\n\r\n";
            replyText += mail.MailFrom + " wrote:\r\n\r\n";
            for(String line: mail.TextContent.split(System.lineSeparator()))
            {
                replyText += "> " + line + "\r\n";
            }
            replyMail.TextContent = replyText;

            // TODO: Add reply header to html

            sendFrame.setContentPane(new NewMailWindow(replyMail).SendPanel);
            sendFrame.pack();
            sendFrame.setVisible(true);
            if (calledExtern) {
                SwingUtilities.getWindowAncestor(panel1).dispose();
            }
        });
        replyAllButton.addActionListener(e -> {
            JFrame sendFrame = new JFrame("Allen antworten auf E-Mail");
            Mail replyallMail = (Mail)DeepCopy.copy(mail);
            replyallMail.Subject = "Re: " + replyallMail.Subject;
            replyallMail.MailTo = replyallMail.MailFrom;
            replyallMail.MailFrom = "";
            replyallMail.MailBCC = "";

            String replyText = "\r\n\r\n";
            replyText += mail.MailFrom + " wrote:\r\n\r\n";
            for(String line: mail.TextContent.split(System.lineSeparator()))
            {
                replyText += "> " + line + "\r\n";
            }
            replyallMail.TextContent = replyText;

            // TODO: Add reply header to html

            sendFrame.setContentPane(new NewMailWindow(replyallMail).SendPanel);
            sendFrame.pack();
            sendFrame.setVisible(true);
            if (calledExtern) {
                SwingUtilities.getWindowAncestor(panel1).dispose();
            }
        });
        saveAttachmentsButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Save attachment(s)");
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fileChooser.setAcceptAllFileFilterUsed(false);
            if (fileChooser.showSaveDialog(panel1) == JFileChooser.APPROVE_OPTION) {
                File saveDir = fileChooser.getSelectedFile();
                for (Attachment attachment: mail.Attachments) {
                    try {
                        attachment.SaveAttachement(saveDir.getAbsolutePath());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    void setMail(Mail mail) {

        this.mail = mail;

        if (mail.HTMLContent != null) {
            System.out.println(mail.HTMLContent);
            Platform.runLater(() -> webView.getEngine().loadContent(mail.HTMLContent));
        }

        if (mail.TextContent != null)
            taTextContent.setText(mail.TextContent);

        if (mail.Subject != null)
            lblSubject.setText(mail.Subject);

        if (mail.MailFrom != null)
            lblFrom.setText(mail.MailFrom);

        if (mail.MailTo != null)
            lblTo.setText(mail.MailTo);

        if (mail.MailCC != null && !mail.MailCC.equals("")) {
            lblCC.setText(mail.MailCC);
            lblCC.setVisible(true);
            lblCCTitle.setVisible(true);
        }
        else {
            lblCC.setVisible(false);
            lblCCTitle.setVisible(false);
        }

        if (mail.MailSent != null)
            lblSentDate.setText(DateFormatter.getNiceDateTimeString(mail.MailSent));

        taTextContent.setCaretPosition(0);

        if (mail.Attachments.size() > 0)
            attachmentPanel.setVisible(true);
        else
            attachmentPanel.setVisible(false);
    }

    private void createUIComponents() {

        //javafx panel initialisation for WebView
        jfxPanelHtml = new JFXPanel();
        Platform.runLater(() -> {
            final AnchorPane anchorPane = new AnchorPane();
            webView = new WebView();

            //Set Layout Constraint
            AnchorPane.setTopAnchor(webView, 0.0);
            AnchorPane.setBottomAnchor(webView, 0.0);
            AnchorPane.setLeftAnchor(webView, 0.0);
            AnchorPane.setRightAnchor(webView, 0.0);

            //Add WebView to AnchorPane
            anchorPane.getChildren().add(webView);

            webView.getEngine().getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
                // catch the webview has successfully loaded event and add listener to links
                if (newValue == Worker.State.SUCCEEDED) {
                    // note next classes are from org.w3c.dom domain
                    EventListener listener = (org.w3c.dom.events.Event evt) -> {
                        String href = ((HTMLAnchorElement)evt.getCurrentTarget()).getHref();

                        // check if mail
                        if (Validator.isValidMail(href)) {
                            // open up a new send mail frame
                            JFrame sendFrame = new JFrame("Mail senden");
                            sendFrame.setContentPane(new NewMailWindow(new Mail(href)).SendPanel);
                            sendFrame.pack();
                            sendFrame.setVisible(true);
                        }
                        else if (Validator.isValidURL(href)) {
                            // open up the link in the default browser
                            DesktopUtils.getInstance().openWebpage(href);
                        }
                        evt.preventDefault();
                    };

                    Document doc = webView.getEngine().getDocument();
                    NodeList lista = doc.getElementsByTagName("a");
                    System.out.println("Link count in HTML: "+ lista.getLength());
                    for (int i=0; i<lista.getLength(); i++)
                        ((EventTarget)lista.item(i)).addEventListener("click", listener, false);
                }
            });
            jfxPanelHtml.setScene(new Scene(anchorPane));
        });
    }
}
