# TODO-list

## Must have features
* Sending of mail - *done*
* Folder management - *in progress*
* Moving of mail(s) to folder - *done*
* Mail window form - *done*
* Settings dialog - *done*
* Periodical receiving of mails - *done*

## GUI
* Proper views for attachements - *in progress*
* Nice view for email list - *in progress*
* Folder-Symbol for mail folders - *done*

## Mail handling
* Add mail at correct position in list - *open*

## Optimazitation
* maillist changed event should only send the really changed positions - *open*

## Future features
* HTML rendering of mails - *in progress*
